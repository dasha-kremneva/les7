import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class PluginManager {

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    private final String pluginRootDirectory;

    public Plugin load(String pluginName, String pluginClassName) {

        Plugin instance = null;

        try {
            URL[] urls = {new URL("jar:file:" + pluginRootDirectory + pluginName +"-1.0-SNAPSHOT.jar" + "!/")};
            URLClassLoader classLoader = new URLClassLoader(urls, ClassLoader.getSystemClassLoader());
            Class pluginClass = classLoader.loadClass(pluginClassName);
            instance = (Plugin) pluginClass.newInstance();

        } catch (MalformedURLException|ClassNotFoundException|InstantiationException|IllegalAccessException e) {
            e.printStackTrace();
        }
        return instance;
    }
}

